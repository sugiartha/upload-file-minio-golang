package main

import (
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func main() {
	endpoint := "localhost:9000"
	accessKeyID := "C2FU3xnAH0Iwl1rI"
	secretAccessKey := "iu9uG0TNDNYz6g74nAbSIBGDzWRY0eLK"
	useSSL := false

	// Initialize minio client object.
	minioClient, _ := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	//if err != nil {
	//	log.Fatalln(err)
	//}

	log.Printf("%#v\n", minioClient) // minioClient is now set up
}
